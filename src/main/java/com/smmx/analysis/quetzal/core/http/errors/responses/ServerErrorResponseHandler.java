/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.errors.responses;

import io.javalin.http.Context;
import io.javalin.http.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Osvaldo Miguel Colin
 */
public class ServerErrorResponseHandler implements ExceptionHandler<ServerErrorResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerErrorResponseHandler.class);

    @Override
    public void handle(ServerErrorResponse error, Context ctx) {
        ctx.status(error.httpStatus());
        ctx.result(error.httpBody());

        if (error.getSource() != null) {
            LOGGER.error(error.getClue(), error.getSource());
        } else {
            LOGGER.error(error.getClue());
        }
    }

}
