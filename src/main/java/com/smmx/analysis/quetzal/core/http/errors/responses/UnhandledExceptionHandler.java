/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.errors.responses;

import io.javalin.http.Context;
import io.javalin.http.ExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Osvaldo Miguel Colin
 */
public class UnhandledExceptionHandler implements ExceptionHandler<Exception> {

    // LOG
    private static final Logger LOGGER = LoggerFactory.getLogger(UnhandledExceptionHandler.class);

    // EXECUTE
    @Override
    public void handle(Exception ex, Context ctx) {
        ServerErrorResponse api_ex = new ServerErrorResponse(ex);
        ctx.status(api_ex.httpStatus());
        ctx.result(api_ex.httpBody());

        // LOG
        LOGGER.error("Unhandled exception.", ex);
    }

}
