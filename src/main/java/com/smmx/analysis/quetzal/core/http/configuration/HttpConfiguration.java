/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.configuration;

import com.smmx.maria.commons.boot.AppConfiguration;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.*;

/**
 * @author Osvaldo Miguel Colin
 */
public class HttpConfiguration {

    // SINGLETON
    private static HttpConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static HttpConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new HttpConfiguration();
        }

        return INSTANCE;
    }

    // MEMBER
    private final String base_path;

    private final Set<ConnectorConfiguration> connectors;

    private HttpConfiguration() {
        Map<String, Object> api_section = dp()
            .require("http")
            .asDictionary()
            .notNull()
            .apply(AppConfiguration.getInstance());

        this.base_path = dp()
            .require("base_path")
            .asString()
            .notNull()
            .apply(api_section);

        this.connectors = dp()
            .get("connectors")
            .asList()
            .notNull()
            .map(list -> {
                if (list.isEmpty()) {
                    return list(dict(
                        "host", "0.0.0.0",
                        "port", 8080
                    ));
                }

                return list;
            })
            .apply(api_section)
            .stream()
            .map(vp()
                .asDictionary()
                .notNull()
                .map(ConnectorConfiguration::new)
            )
            .collect(Collectors.toSet());
    }

    public String getBasePath() {
        return base_path;
    }

    public Set<ConnectorConfiguration> getConnectors() {
        return connectors;
    }

    // CLASSES
    public class ConnectorConfiguration {

        // MEMBER
        private final String host;
        private final Integer port;

        public ConnectorConfiguration(Map<String, Object> bind) {
            this.host = dp()
                .require("host")
                .asString()
                .notNull()
                .apply(bind);

            this.port = dp()
                .require("port")
                .asInteger()
                .notNull()
                .apply(bind);
        }

        public String getHost() {
            return host;
        }

        public Integer getPort() {
            return port;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 83 * hash + Objects.hashCode(this.host);
            hash = 83 * hash + Objects.hashCode(this.port);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }

            if (obj == null) {
                return false;
            }

            if (getClass() != obj.getClass()) {
                return false;
            }

            final ConnectorConfiguration other = (ConnectorConfiguration) obj;

            if (!Objects.equals(this.host, other.host)) {
                return false;
            }

            return Objects.equals(this.port, other.port);
        }

    }

}
