/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers;

import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;

import java.util.HashSet;
import java.util.Set;

import static com.smmx.analysis.quetzal.core.http.handlers.ClientScopes.ROLE_ANYONE;
import static com.smmx.maria.commons.MariaCommons.buuid;

/**
 * @author Osvaldo Miguel Colin
 */
public class ClientProfile {

    private boolean is_anonymous;
    private byte[] session_id;
    private byte[] client_id;
    private final Set<Scope> scopes;

    public ClientProfile(boolean is_anonymous) {
        this.is_anonymous = is_anonymous;
        this.session_id = buuid();
        this.client_id = null;
        this.scopes = new HashSet();
    }

    public void clear() {
        is_anonymous = true;
        session_id = buuid();
        client_id = null;
        scopes.clear();
    }

    // GETTERS AND SETTERS
    public boolean isAnonymous() {
        return is_anonymous;
    }

    public byte[] getSessionId() {
        if (session_id == null) {
            session_id = buuid();
        }

        return session_id;
    }

    public void setSessionId(byte[] session_id) {
        this.session_id = session_id;
    }

    public byte[] getClientId() {
        return client_id;
    }

    public void setClientId(byte[] client_id) {
        this.client_id = client_id;
    }

    public Set<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(Set<Scope> scopes) {
        this.scopes.clear();
        this.scopes.addAll(scopes);
    }

    // EXTRAS
    public boolean hasAnyScope(Set<Scope> scopes) {
        // SHORT-CIRCUIT
        if (scopes.contains(ROLE_ANYONE)) {
            return true;
        }

        // CHECK
        return scopes.stream().anyMatch(a_scope -> {
            return this.scopes.stream().anyMatch(role -> {
                return a_scope.toString()
                    .equalsIgnoreCase(
                        role.toString()
                    );
            });
        });
    }

}
