/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.sql.builders;

import com.smmx.maria.commons.database.sql.SQL;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.smmx.maria.commons.MariaCommons.sf;
import static com.smmx.maria.commons.MariaCommons.sql;

/**
 * @author Osvaldo Miguel Colin
 */
public class SetSQLBuilder {

    private final List<SQLField> fields;

    public SetSQLBuilder() {
        fields = new ArrayList<>();
    }

    public SetSQLBuilder registerField(String paramname, Class<?> datatype) {
        return this.registerField(paramname, paramname, datatype);
    }

    public SetSQLBuilder registerField(String sqlname, String paramname, Class<?> datatype) {
        fields.add(new SQLField(sqlname, paramname, datatype));
        return this;
    }

    public SQL getSetSQL(Set<String> parameters) {
        List<SQLField> available_fields = fields.stream()
            .filter(f -> parameters.contains(f.getParamName()))
            .collect(Collectors.toList());

        if (available_fields.isEmpty()) {
            return null;
        }

        SQL sql = sql(
            available_fields.stream()
                .map(field -> sf("`%s`=:%s", field.getSQLName(), field.getParamName()))
                .collect(Collectors.joining(","))
        );

        available_fields.forEach(field -> {
            sql.registerParameter(field.getParamName(), field.getDatatype());
        });

        return sql;
    }

}
