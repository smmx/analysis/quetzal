/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers.errors;

/**
 * @author Osvaldo Miguel Colin
 */
public class RequiresLoginErrorResponse extends AuthorizationErrorResponse {

    // STATIC
    private static final int ERROR_CODE = 10;

    // CLASS
    public RequiresLoginErrorResponse() {
        super(ERROR_CODE, "Login required.", 401);

        // DETAILS
        putDetail("solution", "login");
    }

}
