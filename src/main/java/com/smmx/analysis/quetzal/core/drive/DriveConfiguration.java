/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.drive;

import com.smmx.maria.commons.boot.AppConfiguration;
import com.smmx.maria.commons.boot.AppRuntime;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class DriveConfiguration {

    // SINGLETON
    private static DriveConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static DriveConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DriveConfiguration();
        }

        return INSTANCE;
    }

    // MEMBER
    private final Path root;

    private DriveConfiguration() {
        Map<String, Object> drive_section = dp()
            .get("drive", Collections.emptyMap())
            .asDictionary()
            .notNull()
            .apply(AppConfiguration.getInstance());

        //////////////////////////CONFIG////////////////////////////////////////
        this.root = dp()
            .get("root", "drive")
            .asString()
            .notNull()
            .map(p -> {
                if (p.startsWith("/")) {
                    return FileSystems.getDefault().getPath(p);
                }

                return AppRuntime.getInstance().home().resolve(p);
            })
            .apply(drive_section);
    }

    public Path getRoot() {
        return root;
    }

}
