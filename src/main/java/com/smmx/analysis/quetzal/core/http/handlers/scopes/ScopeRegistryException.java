/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers.scopes;

/**
 * @author omiguelc
 */
public class ScopeRegistryException extends RuntimeException {

    public ScopeRegistryException(String msg) {
        super(msg);
    }

}
