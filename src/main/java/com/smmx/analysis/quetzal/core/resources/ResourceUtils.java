/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.resources;

import java.nio.file.Path;

/**
 * @author omiguelc
 */
public class ResourceUtils {

    public static Path resources() {
        return ResourcesConfiguration.getInstance().getRoot();
    }

}
