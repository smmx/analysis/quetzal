/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.database.builders;

import java.util.List;

/**
 * @author Osvaldo Miguel Colin
 */
public class DBObjectPrivileges {

    private final String objectname;
    private final List<DBPrivilege> privileges;

    public DBObjectPrivileges(String objectname, List<DBPrivilege> privileges) {
        this.objectname = objectname;
        this.privileges = privileges;
    }

    public String getObjectName() {
        return objectname;
    }

    public List<DBPrivilege> getPrivileges() {
        return privileges;
    }

}
