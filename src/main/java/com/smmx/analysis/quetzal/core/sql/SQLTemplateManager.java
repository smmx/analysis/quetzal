/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.sql;


import com.smmx.analysis.quetzal.core.functional.Factory;
import com.smmx.maria.commons.database.sql.SQLTemplate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author omiguelc
 */
public class SQLTemplateManager {

    // SINGLETON
    private static SQLTemplateManager INSTANCE;

    static {
        INSTANCE = null;
    }

    public static SQLTemplateManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SQLTemplateManager();
        }

        return INSTANCE;
    }

    // MEMBER
    private final Map<String, Class<?>> aliases;
    private final Map<String, Factory<SQLTemplate>> templates;

    public SQLTemplateManager() {
        this.aliases = new HashMap<>();
        this.templates = new HashMap<>();

        registerDatatype("boolean", Boolean.class);
        registerDatatype("binary", byte[].class);
        registerDatatype("integer", Integer.class);
        registerDatatype("float", Float.class);
        registerDatatype("double", Double.class);
        registerDatatype("long", Long.class);
        registerDatatype("decimal", BigDecimal.class);
        registerDatatype("string", String.class);
        registerDatatype("date", Date.class);
        registerDatatype("datetime", Date.class);
        registerDatatype("sql_blob", java.sql.Blob.class);
        registerDatatype("sql_clob", java.sql.Clob.class);
        registerDatatype("sql_nclob", java.sql.NClob.class);
        registerDatatype("sql_date", java.sql.Date.class);
        registerDatatype("sql_timestamp", java.sql.Timestamp.class);
    }

    public Class<?> getDatatype(String alias) {
        return aliases.get(alias);
    }

    public void registerDatatype(String alias, Class<?> clazz) {
        aliases.put(alias, clazz);
    }

    public SQLTemplate getTemplate(String name) {
        return templates.get(name)
            .newInstance();
    }

    public void registerTemplate(String name, Factory<SQLTemplate> tf) {
        templates.put(name, tf);
    }

}
