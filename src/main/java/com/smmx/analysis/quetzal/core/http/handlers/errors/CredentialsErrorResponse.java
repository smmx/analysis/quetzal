/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers.errors;

/**
 * @author Osvaldo Miguel Colin
 */
public class CredentialsErrorResponse extends LoginErrorResponse {

    // STATIC
    private static final int ERROR_CODE = 30;

    // CLASS
    public CredentialsErrorResponse(int code, String message) {
        super(ERROR_CODE + code, message, 401);
    }

    public CredentialsErrorResponse(String message) {
        super(ERROR_CODE, message, 401);
    }

}
