package com.smmx.analysis.quetzal.core.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64Utils {

    public static byte[] urlDecodeNoPadding(String src) {
        int mod4 = src.length() % 4;

        if (mod4 == 1) {
            throw new IllegalArgumentException("Malformed string.");
        } else if (mod4 == 2) {
            src += "==";
        } else if (mod4 == 3) {
            src += "=";
        }

        return Base64.getUrlDecoder().decode(src);
    }

    public static String urlEncodeToStringNoPadding(String src) {
        return Base64.getUrlEncoder().encodeToString(
            src.getBytes(StandardCharsets.UTF_8)
        ).replaceAll("=", "");
    }

    public static String urlEncodeToStringNoPadding(byte[] bytes) {
        return Base64.getUrlEncoder().encodeToString(bytes)
            .replaceAll("=", "");
    }

}
