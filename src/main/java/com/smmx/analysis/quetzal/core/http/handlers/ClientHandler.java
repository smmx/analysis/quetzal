/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers;

import com.smmx.analysis.quetzal.core.http.handlers.errors.UnauthorizedErrorResponse;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;
import io.javalin.http.Context;
import io.javalin.http.Handler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Osvaldo Miguel Colin
 */
public abstract class ClientHandler<C extends ClientContext<P>, P extends ClientProfile> implements Handler {

    private final List<AuthenticationMethod<P>> authentication_methods;
    private final Set<Scope> permitted_scopes;
    private final Set<Scope> reject_scopes;

    public ClientHandler() {
        this.authentication_methods = new ArrayList<>();
        this.permitted_scopes = new HashSet<>();
        this.reject_scopes = new HashSet<>();
    }

    public abstract P createAnonymousProfile();

    public abstract C createContext(Context ctx, P profile);

    public ClientHandler registerAuthenticationMethod(AuthenticationMethod<P> method) {
        this.authentication_methods.add(method);
        return this;
    }

    public ClientHandler permitScope(Scope scope) {
        this.permitted_scopes.add(scope);
        return this;
    }

    public ClientHandler rejectScope(Scope scope) {
        this.reject_scopes.add(scope);
        return this;
    }

    @Override
    public final void handle(Context ctx) throws Exception {
        // CREATE PROFILE
        P client_profile = createAnonymousProfile();

        // AUTHENTICATE
        for (AuthenticationMethod<P> method : authentication_methods) {
            P authenticated_profile = method.authenticate(ctx);

            if (authenticated_profile != null) {
                client_profile = authenticated_profile;
                break;
            }
        }

        // CREATE SECURE CONTEXT
        C client_ctx = createContext(ctx, client_profile);

        // CHECK ROLES
        if (!client_profile.hasAnyScope(permitted_scopes)) {
            throw new UnauthorizedErrorResponse();
        }

        // CHECK ROLES
        if (client_profile.hasAnyScope(reject_scopes)) {
            throw new UnauthorizedErrorResponse();
        }

        // HANDLE
        handle(client_ctx);
    }

    public abstract void handle(C ctx) throws Exception;

}
