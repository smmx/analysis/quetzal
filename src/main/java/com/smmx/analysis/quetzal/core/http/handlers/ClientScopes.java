package com.smmx.analysis.quetzal.core.http.handlers;


import com.smmx.analysis.quetzal.core.http.handlers.scopes.Role;

public class ClientScopes {

    public static final Role ROLE_ANYONE = ClientScopeRegistry.getInstance().registerRole("anyone", true);

}
