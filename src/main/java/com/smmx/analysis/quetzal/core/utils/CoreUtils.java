package com.smmx.analysis.quetzal.core.utils;


import com.smmx.analysis.quetzal.core.http.handlers.scopes.Privilege;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Role;
import com.smmx.analysis.quetzal.core.http.handlers.scopes.Scope;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class CoreUtils {

    public static Set<Scope> scopes(Scope... scopes) {
        return Arrays.stream(scopes).collect(Collectors.toSet());
    }

    public static Set<Role> roles(Role... roles) {
        return Arrays.stream(roles).collect(Collectors.toSet());
    }

    public static Set<Privilege> privileges(Privilege... privileges) {
        return Arrays.stream(privileges).collect(Collectors.toSet());
    }

}
