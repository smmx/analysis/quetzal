package com.smmx.analysis.quetzal.core.http.handlers;

import com.smmx.analysis.quetzal.core.http.HttpUtils;
import com.smmx.maria.commons.api.APIResponse;
import io.javalin.core.security.BasicAuthCredentials;
import io.javalin.core.validation.Validator;
import io.javalin.http.Context;
import io.javalin.http.UploadedFile;
import kotlin.jvm.JvmOverloads;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public abstract class ClientContext<P extends ClientProfile> {

    // CONTEXT
    private final Context ctx;

    // PROFILE
    private final P profile;

    public ClientContext(Context ctx, P profile) {
        this.ctx = ctx;
        this.profile = profile;
    }

    // ADITIONAL INFO
    public P getProfile() {
        return profile;
    }

    // CUSTOM
    public Map<String, Object> bodyParamMap() {
        return HttpUtils.bodyParamMap(ctx);
    }

    public Map<String, Object> multiformParamMap() {
        return HttpUtils.multiformParamMap(ctx);
    }

    public ClientContext result(Map<String, Object> payload) {
        HttpUtils.writeDictionary(ctx, payload);
        return this;
    }

    public ClientContext result(APIResponse response) {
        HttpUtils.writeAPIResponse(ctx, response);
        return this;
    }

    // DELEGATES
    public HttpServletRequest getHttpServletRequest() {
        return ctx.req;
    }

    public HttpServletResponse getHttpServletResponse() {
        return ctx.res;
    }

    public <T> T appAttribute(@NotNull Class<T> clazz) {
        return ctx.appAttribute(clazz);
    }

    @Nullable
    public <T> T attribute(@NotNull String key) {
        return ctx.attribute(key);
    }

    public void attribute(@NotNull String key, @Nullable Object value) {
        ctx.attribute(key, value);
    }

    @NotNull
    public <T> Map<String, T> attributeMap() {
        return ctx.attributeMap();
    }

    @Nullable
    public BasicAuthCredentials basicAuthCredentials() {
        return ctx.basicAuthCredentials();
    }

    @NotNull
    public String body() {
        return ctx.body();
    }

    @NotNull
    public byte[] bodyAsBytes() {
        return ctx.bodyAsBytes();
    }

    public <T> T bodyAsClass(@NotNull Class<T> clazz) {
        return ctx.bodyAsClass(clazz);
    }

    @NotNull
    public <T> Validator<T> bodyValidator(@NotNull Class<T> clazz) {
        return ctx.bodyValidator(clazz);
    }

    public void clearCookieStore() {
        ctx.clearCookieStore();
    }

    public int contentLength() {
        return ctx.contentLength();
    }

    @Nullable
    public String contentType() {
        return ctx.contentType();
    }

    @NotNull
    public ClientContext contentType(@NotNull String contentType) {
        ctx.contentType(contentType);
        return this;
    }

    @NotNull
    public String contextPath() {
        return ctx.contextPath();
    }

    @NotNull
    public ClientContext cookie(@NotNull Cookie cookie) {
        ctx.cookie(cookie);
        return this;
    }

    @Nullable
    public String cookie(@NotNull String name) {
        return ctx.cookie(name);
    }

    @JvmOverloads
    @NotNull
    public ClientContext cookie(@NotNull String name, @NotNull String value, int maxAge) {
        ctx.cookie(name, value, maxAge);
        return this;
    }

    @JvmOverloads
    @NotNull
    public ClientContext cookie(@NotNull String name, @NotNull String value) {
        ctx.cookie(name, value);
        return this;
    }

    @NotNull
    public Map<String, String> cookieMap() {
        return ctx.cookieMap();
    }

    public <T> T cookieStore(@NotNull String key) {
        return ctx.cookieStore(key);
    }

    public void cookieStore(@NotNull String key, @NotNull Object value) {
        ctx.cookieStore(key, value);
    }

    @NotNull
    public String endpointHandlerPath() {
        return ctx.endpointHandlerPath();
    }

    @JvmOverloads
    @NotNull
    public <T> Validator<T> formParam(@NotNull String key, @NotNull Class<T> clazz) {
        return ctx.formParam(key, clazz);
    }

    @JvmOverloads
    @NotNull
    public <T> Validator<T> formParam(@NotNull String key, @NotNull Class<T> clazz, @Nullable String s) {
        return ctx.formParam(key, clazz, s);
    }

    @JvmOverloads
    @Nullable
    public String formParam(@NotNull String key, @Nullable String s) {
        return ctx.formParam(key, s);
    }

    @JvmOverloads
    @Nullable
    public String formParam(@NotNull String key) {
        return ctx.formParam(key);
    }

    @NotNull
    public Map<String, List<String>> formParamMap() {
        return ctx.formParamMap();
    }

    @NotNull
    public List<String> formParams(@NotNull String key) {
        return ctx.formParams(key);
    }

    @NotNull
    public String fullUrl() {
        return ctx.fullUrl();
    }

    @Nullable
    public String header(@NotNull String header) {
        return ctx.header(header);
    }

    @NotNull
    public ClientContext header(@NotNull String name, @NotNull String value) {
        ctx.header(name, value);
        return this;
    }

    @NotNull
    public Map<String, String> headerMap() {
        return ctx.headerMap();
    }

    @Nullable
    public String host() {
        return ctx.host();
    }

    @NotNull
    public ClientContext html(@NotNull String html) {
        ctx.html(html);
        return this;
    }

    @NotNull
    public String ip() {
        return ctx.ip();
    }

    public boolean isMultipart() {
        return ctx.isMultipart();
    }

    public boolean isMultipartFormData() {
        return ctx.isMultipartFormData();
    }

    @NotNull
    public ClientContext json(@NotNull CompletableFuture<?> future) {
        ctx.json(future);
        return this;
    }

    @NotNull
    public ClientContext json(@NotNull Object obj) {
        ctx.json(obj);
        return this;
    }

    @NotNull
    public String matchedPath() {
        return ctx.matchedPath();
    }

    @NotNull
    public String method() {
        return ctx.method();
    }

    @NotNull
    public String path() {
        return ctx.path();
    }

    @NotNull
    public String pathParam(@NotNull String key) {
        return ctx.pathParam(key);
    }

    @NotNull
    public <T> Validator<T> pathParam(@NotNull String key, @NotNull Class<T> clazz) {
        return ctx.pathParam(key, clazz);
    }

    @NotNull
    public Map<String, String> pathParamMap() {
        return ctx.pathParamMap();
    }

    public int port() {
        return ctx.port();
    }

    @NotNull
    public String protocol() {
        return ctx.protocol();
    }

    @JvmOverloads
    @NotNull
    public <T> Validator<T> queryParam(@NotNull String key, @NotNull Class<T> clazz, @Nullable String s) {
        return ctx.queryParam(key, clazz, s);
    }

    @JvmOverloads
    @NotNull
    public <T> Validator<T> queryParam(@NotNull String key, @NotNull Class<T> clazz) {
        return ctx.queryParam(key, clazz);
    }

    @JvmOverloads
    @Nullable
    public String queryParam(@NotNull String key) {
        return ctx.queryParam(key);
    }

    @JvmOverloads
    @Nullable
    public String queryParam(@NotNull String key, @Nullable String s) {
        return ctx.queryParam(key, s);
    }

    @NotNull
    public Map<String, List<String>> queryParamMap() {
        return ctx.queryParamMap();
    }

    @NotNull
    public List<String> queryParams(@NotNull String key) {
        return ctx.queryParams(key);
    }

    @Nullable
    public String queryString() {
        return ctx.queryString();
    }

    @JvmOverloads
    public void redirect(@NotNull String location, int httpStatusCode) {
        ctx.redirect(location, httpStatusCode);
    }

    @JvmOverloads
    public void redirect(@NotNull String location) {
        ctx.redirect(location);
    }

    public void register(@NotNull Class<?> clazz, @NotNull Object value) {
        ctx.register(clazz, value);
    }

    @JvmOverloads
    @NotNull
    public ClientContext removeCookie(@NotNull String name, @Nullable String path) {
        ctx.removeCookie(name, path);
        return this;
    }

    @JvmOverloads
    @NotNull
    public ClientContext removeCookie(@NotNull String name) {
        ctx.removeCookie(name);
        return this;
    }

    @JvmOverloads
    @NotNull
    public ClientContext render(@NotNull String filePath) {
        ctx.render(filePath);
        return this;
    }

    @JvmOverloads
    @NotNull
    public ClientContext render(@NotNull String filePath, @NotNull Map<String, ?> model) {
        ctx.render(filePath, model);
        return this;
    }

    @NotNull
    public ClientContext result(@NotNull InputStream resultStream) {
        ctx.result(resultStream);
        return this;
    }

    @NotNull
    public ClientContext result(@NotNull CompletableFuture<?> future) {
        ctx.result(future);
        return this;
    }

    @NotNull
    public ClientContext result(@NotNull String resultString) {
        ctx.result(resultString);
        return this;
    }

    @Nullable
    public CompletableFuture<?> resultFuture() {
        return ctx.resultFuture();
    }

    @Nullable
    public InputStream resultStream() {
        return ctx.resultStream();
    }

    @Nullable
    public String resultString() {
        return ctx.resultString();
    }

    @NotNull
    public String scheme() {
        return ctx.scheme();
    }

    @Nullable
    public <T> T sessionAttribute(@NotNull String key) {
        return ctx.sessionAttribute(key);
    }

    public void sessionAttribute(@NotNull String key, @Nullable Object value) {
        ctx.sessionAttribute(key, value);
    }

    @NotNull
    public <T> Map<String, T> sessionAttributeMap() {
        return ctx.sessionAttributeMap();
    }

    public int status() {
        return ctx.status();
    }

    @NotNull
    public ClientContext status(int statusCode) {
        ctx.status(statusCode);
        return this;
    }

    @Nullable
    public UploadedFile uploadedFile(@NotNull String fileName) {
        return ctx.uploadedFile(fileName);
    }

    @NotNull
    public List<UploadedFile> uploadedFiles(@NotNull String fileName) {
        return ctx.uploadedFiles(fileName);
    }

    @NotNull
    public String url() {
        return ctx.url();
    }

    public <T> T use(@NotNull Class<T> clazz) {
        return ctx.use(clazz);
    }

    @Nullable
    public String userAgent() {
        return ctx.userAgent();
    }

}
