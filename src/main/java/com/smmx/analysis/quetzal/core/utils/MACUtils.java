package com.smmx.analysis.quetzal.core.utils;

import com.smmx.analysis.quetzal.core.http.errors.ServerException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MACUtils {

    private static final String HMAC_SHA256 = "HmacSHA256";

    public static String sign(String to_sign, byte[] secret) {
        try {
            Mac mac = Mac.getInstance(HMAC_SHA256);

            SecretKeySpec key_spec = new SecretKeySpec(secret, HMAC_SHA256);

            mac.init(key_spec);

            byte[] mac_bytes = mac.doFinal(
                to_sign.getBytes(StandardCharsets.UTF_8)
            );

            return Base64Utils.urlEncodeToStringNoPadding(mac_bytes);
        } catch (InvalidKeyException | NoSuchAlgorithmException ex) {
            throw new ServerException("Failed to calculate " + HMAC_SHA256 + ".", ex);
        }
    }
}
