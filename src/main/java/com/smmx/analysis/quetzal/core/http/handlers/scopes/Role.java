package com.smmx.analysis.quetzal.core.http.handlers.scopes;

public class Role extends Scope {

    private final boolean obscure;

    public Role(String name, boolean obscure) {
        super(name);

        this.obscure = obscure;
    }

    public boolean isObscure() {
        return obscure;
    }

}
