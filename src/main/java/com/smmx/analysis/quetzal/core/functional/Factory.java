/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.functional;

/**
 * @param <T>
 * @author omiguelc
 */
public interface Factory<T> {

    T newInstance();

}
