/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers.errors;

import com.smmx.analysis.quetzal.core.http.errors.responses.ClientErrorResponse;

/**
 * @author Osvaldo Miguel Colin
 */
public class LoginErrorResponse extends ClientErrorResponse {

    // STATIC
    private static final int ERROR_CODE = 2100;

    // CLASS
    public LoginErrorResponse(int code, String message) {
        super(ERROR_CODE + code, message, 401);
    }

    public LoginErrorResponse(int code, String message, int status) {
        super(ERROR_CODE + code, message, status);
    }

}
