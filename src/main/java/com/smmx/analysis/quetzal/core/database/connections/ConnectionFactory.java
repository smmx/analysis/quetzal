/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.database.connections;

import com.smmx.analysis.quetzal.core.database.configuration.ConnectionConfiguration;
import com.smmx.analysis.quetzal.core.database.configuration.DatabaseConfiguration;
import com.smmx.maria.commons.database.connections.ConnectionShield;
import org.apache.commons.pool2.KeyedPooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import java.util.NoSuchElementException;

import static com.smmx.maria.commons.MariaCommons.dict;

/**
 * @author Osvaldo Miguel Colin
 */
public class ConnectionFactory implements KeyedPooledObjectFactory<String, ConnectionShield> {

    @Override
    public PooledObject<ConnectionShield> makeObject(String k) {
        ConnectionConfiguration config = DatabaseConfiguration.getInstance().getConnections().get(k);

        if (config == null) {
            throw new NoSuchElementException("Config doesn't exist.");
        }

        ConnectionShield connection = new ConnectionShield(
            config.getURL(),
            dict(
                "user", config.getUsername(),
                "password", config.getPassword()
            )
        );

        return new DefaultPooledObject<>(connection);
    }

    @Override
    public void destroyObject(String k, PooledObject<ConnectionShield> po) throws Exception {
        po.getObject().close();
    }

    @Override
    public boolean validateObject(String k, PooledObject<ConnectionShield> po) {
        return po.getObject().isValid(5);
    }

    @Override
    public void activateObject(String k, PooledObject<ConnectionShield> po) throws Exception {
        // DO NOTHING
    }

    @Override
    public void passivateObject(String k, PooledObject<ConnectionShield> po) throws Exception {
        if (po.getObject().isTransactionOpen()) {
            po.getObject().getTransactionHandler()
                .close(false);
        }
    }

}
