/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.drive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;

/**
 * @author omiguelc
 */
public class DriveUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(DriveUtils.class);

    public static Path drive() {
        return DriveConfiguration.getInstance().getRoot();
    }

    public static boolean touchDirectory(String path) {
        File dir = drive()
            .resolve(path)
            .toFile();

        if (!dir.exists()) {
            LOGGER.warn("Drive directory doesn't exist.");

            if (dir.mkdirs()) {
                LOGGER.info("Drive directory has been created: {}",
                    DriveConfiguration.getInstance().getRoot().toString()
                );

                return true;
            } else {
                LOGGER.error("The drive directory couldn't be created.");
            }
        }

        if (!dir.isDirectory()) {
            LOGGER.error("The drive directory specified is a file.");

            return false;
        }

        return true;
    }

}
