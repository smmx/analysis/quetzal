/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http;

import com.smmx.analysis.quetzal.core.http.configuration.HttpConfiguration;
import com.smmx.analysis.quetzal.core.http.errors.ServerException;
import com.smmx.analysis.quetzal.core.http.errors.responses.*;
import com.smmx.maria.commons.values.errors.ValueError;
import com.smmx.maria.commons.values.subtypes.DSTException;
import com.smmx.maria.commons.values.types.DTException;
import io.javalin.Javalin;
import io.javalin.core.security.AccessManager;
import io.javalin.core.security.Role;
import io.javalin.http.ExceptionHandler;
import io.javalin.http.Handler;
import io.javalin.http.HttpResponseException;
import io.javalin.http.sse.SseClient;
import io.javalin.http.staticfiles.Location;
import io.javalin.websocket.WsExceptionHandler;
import io.javalin.websocket.WsHandler;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.json.JSONException;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Osvaldo Miguel Colin
 */
public class Http {

    // SINGLETON
    private static Http INSTANCE;

    static {
        INSTANCE = null;
    }

    public static Http getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Http();
        }

        return INSTANCE;
    }

    // MEMBER
    private final Javalin javalin;

    private Http() {
        javalin = Javalin.create(config -> {
            config.server(() -> {
                Server server = new Server();

                // GET HOSTS
                Set<HttpConfiguration.ConnectorConfiguration> connector_configs = new HashSet<>(
                    HttpConfiguration.getInstance().getConnectors()
                );

                // BUILD CONNECTORS
                Connector[] connectors = new Connector[connector_configs.size()];

                connector_configs
                    .stream()
                    .map((connector) -> {
                        ServerConnector serverConnector = new ServerConnector(server);

                        serverConnector.setHost(connector.getHost());
                        serverConnector.setPort(connector.getPort());

                        return serverConnector;
                    })
                    .collect(Collectors.toList())
                    .toArray(connectors);

                // SET CONNECTORS
                server.setConnectors(connectors);

                // RETURN
                return server;
            });

            // CORS
            config.enableCorsForAllOrigins();

            // CONTEXT PATH
            config.contextPath = HttpConfiguration.getInstance().getBasePath();
            config.wsContextPath = HttpConfiguration.getInstance().getBasePath();
        });

        // EXCEPTION HANDLING
        javalin.exception(HttpResponseException.class, new HttpResponseExceptionHandler());
        javalin.exception(JSONException.class, new UnhandledClientErrorResponseHandler());
        javalin.exception(DTException.class, new UnhandledClientErrorResponseHandler());
        javalin.exception(DSTException.class, new UnhandledClientErrorResponseHandler());
        javalin.exception(ValueError.class, new UnhandledClientErrorResponseHandler());

        javalin.exception(ServerErrorResponse.class, new ServerErrorResponseHandler());
        javalin.exception(ErrorResponseException.class, new ErrorResponseExceptionHandler());

        javalin.exception(ServerException.class, new UnhandledExceptionHandler());
        javalin.exception(Exception.class, new UnhandledExceptionHandler());
    }

    public Http start() {
        javalin.start();
        return this;
    }

    public Http stop() {
        javalin.stop();
        return this;
    }

    public int port() {
        return javalin.port();
    }

    public Http attribute(Class clazz, Object obj) {
        javalin.attribute(clazz, obj);
        return this;
    }

    public <T> T attribute(Class<T> clazz) {
        return javalin.attribute(clazz);
    }

    public Http accessManager(AccessManager access_manager) {
        javalin.config.accessManager(access_manager);
        return this;
    }

    public Http addStaticFiles(String class_path) {
        javalin.config.addStaticFiles(class_path);
        return this;
    }

    public Http addStaticFiles(String path, Location location) {
        javalin.config.addStaticFiles(path, location);
        return this;
    }

    public <T extends Exception> Http exception(Class<T> exceptionClass, ExceptionHandler<? super T> exceptionHandler) {
        javalin.exception(exceptionClass, exceptionHandler);
        return this;
    }

    public Http error(int statusCode, Handler handler) {
        javalin.error(statusCode, handler);
        return this;
    }

    public Http error(int statusCode, String contentType, Handler handler) {
        javalin.error(statusCode, contentType, handler);
        return this;
    }

    public Http get(String path, Handler handler) {
        javalin.get(path, handler);
        return this;
    }

    public Http post(String path, Handler handler) {
        javalin.post(path, handler);
        return this;
    }

    public Http put(String path, Handler handler) {
        javalin.put(path, handler);
        return this;
    }

    public Http patch(String path, Handler handler) {
        javalin.patch(path, handler);
        return this;
    }

    public Http delete(String path, Handler handler) {
        javalin.delete(path, handler);
        return this;
    }

    public Http head(String path, Handler handler) {
        javalin.head(path, handler);
        return this;
    }

    public Http options(String path, Handler handler) {
        javalin.options(path, handler);
        return this;
    }

    public Http get(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.get(path, handler, permittedRoles);
        return this;
    }

    public Http post(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.post(path, handler, permittedRoles);
        return this;
    }

    public Http put(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.put(path, handler, permittedRoles);
        return this;
    }

    public Http patch(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.patch(path, handler, permittedRoles);
        return this;
    }

    public Http delete(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.delete(path, handler, permittedRoles);
        return this;
    }

    public Http head(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.head(path, handler, permittedRoles);
        return this;
    }

    public Http options(String path, Handler handler, Set<Role> permittedRoles) {
        javalin.options(path, handler, permittedRoles);
        return this;
    }

    public Http sse(String path, Consumer<SseClient> client) {
        javalin.sse(path, client);
        return this;
    }

    public Http sse(String path, Consumer<SseClient> client, Set<Role> permittedRoles) {
        javalin.sse(path, client, permittedRoles);
        return this;
    }

    public Http before(String path, Handler handler) {
        javalin.before(path, handler);
        return this;
    }

    public Http before(Handler handler) {
        javalin.before(handler);
        return this;
    }

    public Http after(String path, Handler handler) {
        javalin.after(path, handler);
        return this;
    }

    public Http after(Handler handler) {
        javalin.after(handler);
        return this;
    }

    public <T extends Exception> Http wsException(Class<T> exceptionClass, WsExceptionHandler<? super T> exceptionHandler) {
        javalin.wsException(exceptionClass, exceptionHandler);
        return this;
    }

    public Http ws(String path, Consumer<WsHandler> ws) {
        javalin.ws(path, ws);
        return this;
    }

    public Http ws(String path, Consumer<WsHandler> ws, Set<Role> permittedRoles) {
        javalin.ws(path, ws, permittedRoles);
        return this;
    }

    public Http wsBefore(String path, Consumer<WsHandler> wsHandler) {
        javalin.wsBefore(path, wsHandler);
        return this;
    }

    public Http wsBefore(Consumer<WsHandler> wsHandler) {
        javalin.wsBefore(wsHandler);
        return this;
    }

    public Http wsAfter(String path, Consumer<WsHandler> wsHandler) {
        javalin.wsAfter(path, wsHandler);
        return this;
    }

    public Http wsAfter(Consumer<WsHandler> wsHandler) {
        javalin.wsAfter(wsHandler);
        return this;
    }

}
