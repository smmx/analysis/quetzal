/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.errors.responses;

import com.smmx.maria.commons.api.APIResponse;

import java.util.HashMap;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dict;

/**
 * @author Osvaldo Miguel Colin
 */
public class ErrorResponseException extends RuntimeException {

    private final int code;
    private final String message;
    private final Map<String, Object> details;
    private final int status;

    public ErrorResponseException(int code, String message, int status) {
        this.code = code;
        this.message = message;
        this.details = new HashMap<>();
        this.status = status;
    }

    public ErrorResponseException putDetail(String key, Object value) {
        this.details.put(key, value);
        return this;
    }

    public ErrorResponseException putDetails(Map<String, Object> details) {
        this.details.putAll(details);
        return this;
    }

    // GETTERS
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

    // HTTP
    public int httpStatus() {
        return status;
    }

    public String httpBody() {
        return new APIResponse()
            .header("__iserror__", true)
            .payload(dict(
                "code", code,
                "message", message,
                "details", details
            ))
            .toString();
    }
}
