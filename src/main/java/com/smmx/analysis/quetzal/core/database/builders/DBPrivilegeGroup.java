/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.database.builders;


import com.smmx.analysis.quetzal.core.database.configuration.DatabaseConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Osvaldo Miguel Colin
 */
public class DBPrivilegeGroup {

    private final List<String> users;
    private final List<DBObjectPrivileges> object_privileges;

    public DBPrivilegeGroup() {
        this.users = new ArrayList();
        this.object_privileges = new ArrayList();
    }

    public DBPrivilegeGroup addConnection(String connection_name) {
        String user = DatabaseConfiguration.getInstance()
            .getConnections()
            .values()
            .stream()
            .filter(connection -> connection.getName().equals(connection_name))
            .map(connection -> connection.getUsername())
            .findFirst()
            .orElseThrow(NoSuchElementException::new);

        this.users.add(user);

        return this;
    }

    public DBPrivilegeGroup addObjectPrivileges(String objectname, List<DBPrivilege> privileges) {
        objectname = DatabaseConfiguration.getInstance()
            .getSQLDefinitions()
            .get(objectname);

        this.object_privileges.add(new DBObjectPrivileges(
            objectname,
            privileges
        ));

        return this;
    }

    public List<String> getUsers() {
        return users;
    }

    public List<DBObjectPrivileges> getObjectPrivileges() {
        return object_privileges;
    }

}
