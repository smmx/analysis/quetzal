package com.smmx.analysis.quetzal.core.utils;

import com.smmx.analysis.quetzal.core.http.errors.ServerException;

import javax.crypto.KeyGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class SecretUtils {

    public static String generateKey(int size) {
        return Base64Utils.urlEncodeToStringNoPadding(
            SecretUtils.generateSecret(size)
        );
    }

    public static final byte[] generateSecret(int size) {
        SecureRandom secure_random;

        try {
            secure_random = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException ex) {
            throw new ServerException("No strong secure random available.", ex);
        }

        KeyGenerator key_generator;

        try {
            key_generator = KeyGenerator.getInstance("AES");
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("AES key generator should always be available in a Java runtime.", ex);
        }

        key_generator.init(size, secure_random);

        return key_generator.generateKey().getEncoded();
    }

}
