/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.sql.builders;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLField {

    private final String sqlname;
    private final String paramname;
    private final Class<?> datatype;

    public SQLField(String sqlname, Class<?> datatype) {
        this.sqlname = sqlname;
        this.paramname = sqlname;
        this.datatype = datatype;
    }

    public SQLField(String sqlname, String paramname, Class<?> datatype) {
        this.sqlname = sqlname;
        this.paramname = paramname;
        this.datatype = datatype;
    }

    public String getSQLName() {
        return sqlname;
    }

    public String getParamName() {
        return paramname;
    }

    public Class<?> getDatatype() {
        return datatype;
    }

}
