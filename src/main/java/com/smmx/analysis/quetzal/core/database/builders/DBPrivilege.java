/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.database.builders;

/**
 * @author Osvaldo Miguel Colin
 */
public enum DBPrivilege {
    SELECT, INSERT, UPDATE, DELETE
}
