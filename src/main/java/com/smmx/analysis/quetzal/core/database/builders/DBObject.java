/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.database.builders;

import com.smmx.maria.commons.database.connections.ConnectionRunnable;
import com.smmx.maria.commons.database.sql.SQL;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Osvaldo Miguel Colin
 */
public class DBObject {

    private final String name;
    private final SQL statement;

    private final List<ConnectionRunnable> if_exists_stack;
    private final List<ConnectionRunnable> if_not_exists_stack;

    public DBObject(String name, SQL statement) {
        this.name = name;
        this.statement = statement;
        this.if_exists_stack = new ArrayList();
        this.if_not_exists_stack = new ArrayList();
    }

    public String getName() {
        return name;
    }

    public SQL getStatement() {
        return statement;
    }

    public DBObject ifExists(ConnectionRunnable if_exists) {
        this.if_exists_stack.add(if_exists);
        return this;
    }

    public DBObject ifNotExists(ConnectionRunnable if_not_exists) {
        this.if_not_exists_stack.add(if_not_exists);
        return this;
    }

    public List<ConnectionRunnable> getIfExistsStack() {
        return if_exists_stack;
    }

    public List<ConnectionRunnable> getIfNotExistsStack() {
        return if_not_exists_stack;
    }

}
