/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.errors.responses;

import io.javalin.http.Context;
import io.javalin.http.ExceptionHandler;

/**
 * @author Osvaldo Miguel Colin
 */
public class UnhandledClientErrorResponseHandler implements ExceptionHandler<Exception> {

    @Override
    public void handle(Exception ex, Context ctx) {
        ClientErrorResponse api_ex = new ClientErrorResponse(ex);
        ctx.status(api_ex.httpStatus());
        ctx.result(api_ex.httpBody());
    }

}
