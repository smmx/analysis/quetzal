/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.errors.responses;

import io.javalin.http.Context;
import io.javalin.http.ExceptionHandler;
import io.javalin.http.HttpResponseException;

/**
 * @author Osvaldo Miguel Colin
 */
public class HttpResponseExceptionHandler implements ExceptionHandler<HttpResponseException> {

    @Override
    public void handle(HttpResponseException ex, Context ctx) {
        ClientErrorResponse api_ex = new ClientErrorResponse(ex.getMessage(), ex.getStatus());
        ctx.status(api_ex.httpStatus());
        ctx.result(api_ex.httpBody());
    }

}
