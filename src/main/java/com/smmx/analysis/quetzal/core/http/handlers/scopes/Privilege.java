package com.smmx.analysis.quetzal.core.http.handlers.scopes;

public class Privilege extends Scope {

    public Privilege(String name) {
        super("p-" + name);
    }

}
