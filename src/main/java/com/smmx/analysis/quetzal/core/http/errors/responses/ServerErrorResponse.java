/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.errors.responses;

/**
 * @author Osvaldo Miguel Colin
 */
public class ServerErrorResponse extends ErrorResponseException {

    // STATIC
    private static final int ERROR_CODE = 10000;

    // MEMBER
    private final Throwable source;
    private final String clue;

    public ServerErrorResponse(Throwable error) {
        super(ERROR_CODE, "Internal Error.", 500);

        this.source = error;
        this.clue = null;
    }

    public ServerErrorResponse(String clue, Throwable error) {
        super(ERROR_CODE, "Internal Error.", 500);

        this.source = error;
        this.clue = clue;
    }

    public ServerErrorResponse(int status, Throwable error) {
        super(ERROR_CODE, "Internal Error.", status);

        this.source = error;
        this.clue = null;
    }

    public ServerErrorResponse(int status, String clue, Throwable error) {
        super(ERROR_CODE, "Internal Error.", status);

        this.source = error;
        this.clue = clue;
    }

    public ServerErrorResponse(int code, int status, String clue, Throwable error) {
        super(ERROR_CODE + code, "Internal Error.", status);

        this.source = error;
        this.clue = clue;
    }

    public Throwable getSource() {
        return source;
    }

    public String getClue() {
        return clue;
    }

}
