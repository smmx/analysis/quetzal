/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers.scopes;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Osvaldo Miguel Colin
 */
public class ScopeRegistry {

    private final Set<Privilege> privileges;
    private final Set<Role> roles;

    public ScopeRegistry() {
        this.privileges = new HashSet<>();
        this.roles = new HashSet<>();
    }

    public Privilege registerPrivilege(String name) {
        // COLLISION CHECK
        boolean collision = privileges.stream()
            .anyMatch(privilege -> {
                return privilege.getName()
                    .equalsIgnoreCase(name);
            });

        if (collision) {
            throw new ScopeRegistryException("Privilege collision! Privilege \"" + name + "\" has already been registered.");
        }

        // INIT
        Privilege privilege = new Privilege(name);

        // REGISTER
        privileges.add(privilege);

        // RETURN
        return privilege;
    }

    public Role registerRole(String name, boolean obscure) {
        // COLLISION CHECK
        boolean collision = roles.stream()
            .anyMatch(role -> {
                return role.getName()
                    .equalsIgnoreCase(name);
            });

        if (collision) {
            throw new ScopeRegistryException("Role collision! Role \"" + name + "\" has already been registered.");
        }

        // INIT
        Role role = new Role(name, obscure);

        // REGISTER
        roles.add(role);

        // RETURN
        return role;
    }

    public Set<Privilege> getPrivileges() {
        return privileges.stream()
            .collect(Collectors.toSet());
    }

    public Set<String> getPrivilegeNames() {
        return roles.stream()
            .map(Scope::getName)
            .collect(Collectors.toSet());
    }

    public Privilege resolvePrivilege(String name) {
        return privileges.stream()
            .filter(role -> {
                return role.getName()
                    .equalsIgnoreCase(name);
            })
            .findFirst()
            .orElseThrow(() -> new ScopeRegistryException("Privilege \"" + name + "\" doesn't exist."));
    }

    public Set<Role> getRoles() {
        return roles.stream()
            .filter(role -> !role.isObscure())
            .collect(Collectors.toSet());
    }

    public Set<String> getRoleNames() {
        return roles.stream()
            .filter(role -> !role.isObscure())
            .map(Scope::getName)
            .collect(Collectors.toSet());
    }

    public Role resolveRole(String name) {
        return roles.stream()
            .filter(role -> {
                return role.getName()
                    .equalsIgnoreCase(name);
            })
            .findFirst()
            .orElseThrow(() -> new ScopeRegistryException("Role \"" + name + "\" doesn't exist."));
    }

    public Role resolveNonObscureRole(String name) {
        return roles.stream()
            .filter(role -> !role.isObscure())
            .filter(role -> {
                return role.getName()
                    .equalsIgnoreCase(name);
            })
            .findFirst()
            .orElseThrow(() -> new ScopeRegistryException("Role \"" + name + "\" doesn't exist."));
    }

}
