/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers.errors;

/**
 * @author Osvaldo Miguel Colin
 */
public class UnauthorizedErrorResponse extends AuthorizationErrorResponse {

    // STATIC
    private static final int ERROR_CODE = 50;

    // CLASS
    public UnauthorizedErrorResponse() {
        super(ERROR_CODE, "Unauthorized.", 401);
    }

}
