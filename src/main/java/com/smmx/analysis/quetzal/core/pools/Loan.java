/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.pools;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @param <T>
 * @author Osvaldo Miguel Colin
 */
public abstract class Loan<T> implements AutoCloseable {

    private T object;

    public Loan(T object) {
        this.object = object;
    }

    public T getObject() {
        if (this.object == null) {
            throw new IllegalStateException("Loan has been returned");
        }

        return this.object;
    }

    public <R> R with(Function<T, R> function) {
        if (this.object == null) {
            throw new IllegalStateException("Loan has been returned");
        }

        try {
            return function.apply(object);
        } finally {
            this.close();
        }
    }

    public void with(Consumer<T> consumer) {
        if (this.object == null) {
            throw new IllegalStateException("Loan has been returned");
        }

        try {
            consumer.accept(object);
        } finally {
            this.close();
        }
    }

    public abstract void returnObject();

    public abstract void invalidateObject();

    @Override
    public void close() {
        if (this.object != null) {
            // RETURN
            returnObject();

            // UNSET
            this.object = null;
        }
    }
}
