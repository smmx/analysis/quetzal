/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.handlers;

import com.smmx.analysis.quetzal.core.http.handlers.scopes.ScopeRegistry;

/**
 * @author Osvaldo Miguel Colin
 */
public class ClientScopeRegistry extends ScopeRegistry {

    // SINGLETON
    private static ClientScopeRegistry INSTANCE;

    static {
        INSTANCE = null;
    }

    public static ClientScopeRegistry getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClientScopeRegistry();
        }

        return INSTANCE;
    }

    // CLASS
    private ClientScopeRegistry() {
        super();
    }

}
