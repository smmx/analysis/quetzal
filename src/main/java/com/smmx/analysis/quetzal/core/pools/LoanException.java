/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.pools;

/**
 * @author Osvaldo Miguel Colin
 */
public class LoanException extends RuntimeException {

    public LoanException() {
    }

    public LoanException(String msg) {
        super(msg);
    }

    public LoanException(Throwable thrwbl) {
        super(thrwbl);
    }

    public LoanException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

}
