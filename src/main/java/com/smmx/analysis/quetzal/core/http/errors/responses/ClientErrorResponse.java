/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.errors.responses;

/**
 * @author Osvaldo Miguel Colin
 */
public class ClientErrorResponse extends ErrorResponseException {

    // STATIC
    private static final int ERROR_CODE = 20000;

    // MEMBER
    public ClientErrorResponse() {
        super(ERROR_CODE, "Client Error.", 400);
    }

    public ClientErrorResponse(Throwable error) {
        super(ERROR_CODE, "Client Error:" + error.getMessage(), 400);
    }

    public ClientErrorResponse(String message) {
        super(ERROR_CODE, message, 400);
    }

    public ClientErrorResponse(int status) {
        super(ERROR_CODE, "Client Error", status);
    }

    public ClientErrorResponse(String message, int status) {
        super(ERROR_CODE, message, status);
    }

    public ClientErrorResponse(int code, String message, int status) {
        super(ERROR_CODE + code, message, status);
    }

}
