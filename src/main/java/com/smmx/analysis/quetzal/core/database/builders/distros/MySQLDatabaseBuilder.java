/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.database.builders.distros;

import com.smmx.analysis.quetzal.core.database.builders.DBObject;
import com.smmx.analysis.quetzal.core.database.builders.DBObjectPrivileges;
import com.smmx.analysis.quetzal.core.database.builders.DBPrivilege;
import com.smmx.analysis.quetzal.core.database.builders.DBPrivilegeGroup;
import com.smmx.analysis.quetzal.core.pools.Loan;
import com.smmx.maria.commons.database.connections.ConnectionRunnable;
import com.smmx.maria.commons.database.connections.ConnectionShield;
import com.smmx.maria.commons.database.sql.PreparedStatementShield;
import com.smmx.maria.commons.database.sql.SQL;
import com.smmx.maria.commons.database.transactions.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.smmx.analysis.quetzal.QuetzalUtils.connection;
import static com.smmx.maria.commons.MariaCommons.sql;
import static com.smmx.maria.commons.MariaCommons.transaction;

/**
 * @author Osvaldo Miguel Colin
 */
public class MySQLDatabaseBuilder {

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(MySQLDatabaseBuilder.class);

    // MEMBER
    private final List<DBObject> views;
    private final List<DBObject> tables;
    private final List<DBPrivilegeGroup> privilege_groups;

    public MySQLDatabaseBuilder() {
        this.views = new ArrayList();
        this.tables = new ArrayList();
        this.privilege_groups = new ArrayList();
    }

    // GETTERS
    public List<String> getTablenames() {
        return tables.stream().map(DBObject::getName).collect(Collectors.toList());
    }

    public List<String> getViewnames() {
        return views.stream().map(DBObject::getName).collect(Collectors.toList());
    }

    // REGISTER
    public DBObject registerTable(String name, SQL statement) {
        DBObject obj = new DBObject(name, statement);
        tables.add(obj);
        return obj;
    }

    public DBObject registerView(String name, SQL statement) {
        DBObject obj = new DBObject(name, statement);
        views.add(obj);
        return obj;
    }

    public void registerPrivilegeGroup(DBPrivilegeGroup privilege) {
        privilege_groups.add(privilege);
    }

    // SETUP
    public void setup(String setup_connection) {
        // LOG
        LOGGER.info("Database setup start.");

        try (Loan<ConnectionShield> loan = connection(setup_connection)) {
            ////////////////////////////////////////////////////////////////////
            // CREATE TABLES ///////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////
            //
            // LOG
            LOGGER.info("Creating tables.");

            // TRANSACTION
            Transaction table_transaction = transaction((connection) -> {
                for (DBObject table : tables) {
                    createDBObject(table, connection);
                }
            });

            table_transaction.run(loan.getObject());

            // LOG
            LOGGER.info("Tables created.");

            ////////////////////////////////////////////////////////////////////
            // CREATE VIEWS ////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////
            //
            // LOG
            LOGGER.info("Creating views.");

            // TRANSACTION
            Transaction view_transaction = transaction((connection) -> {
                for (DBObject view : views) {
                    createDBObject(view, connection);
                }
            });

            view_transaction.run(loan.getObject());

            // LOG
            LOGGER.info("Views created.");

            ////////////////////////////////////////////////////////////////////
            // SET PRIVILEGES //////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////
            //
            // LOG
            LOGGER.info("Setting privileges.");

            // EXECUTE
            Transaction grant_transaction = transaction((connection) -> {
                for (DBPrivilegeGroup privilege_group : privilege_groups) {
                    for (DBObjectPrivileges object_privileges : privilege_group.getObjectPrivileges()) {
                        for (String user : privilege_group.getUsers()) {
                            grantPrivilege(
                                object_privileges.getPrivileges(),
                                object_privileges.getObjectName(),
                                user,
                                connection
                            );
                        }
                    }
                }
            });

            grant_transaction.run(loan.getObject());

            // LOG
            LOGGER.info("Privileges set.");

            // LOG
            LOGGER.info("Database setup done.");
        } catch (SQLException ex) {
            LOGGER.error("Failed to setup database.", ex);
        }
    }

    public void createDBObject(DBObject obj, ConnectionShield connection) throws SQLException {
        PreparedStatement prepared_statement = obj.getStatement()
            .preparedStatement(connection)
            .getPreparedStatement();

        // EXECUTE
        boolean exists = false;

        try {
            prepared_statement.execute();

            // CHECK WARNING
            SQLWarning warning = prepared_statement.getWarnings();

            if (warning != null) {
                exists = (warning.getErrorCode() == 1050);
            }
        } finally {
            prepared_statement.close();
        }

        if (exists) {
            // LOG
            LOGGER.info("DB object \"{}\" already existed.", obj.getName());

            // POST
            for (ConnectionRunnable runnable : obj.getIfExistsStack()) {
                runnable.run(connection);
            }
        } else {
            // LOG
            LOGGER.info("DB object \"{}\" created.", obj.getName());

            // POST
            for (ConnectionRunnable runnable : obj.getIfNotExistsStack()) {
                runnable.run(connection);
            }
        }
    }

    public void grantPrivilege(List<DBPrivilege> privileges, String object, String user, ConnectionShield connection) throws SQLException {
        PreparedStatementShield prepared_statement = sql(
            "GRANT {% privileges %} ON {% object %} TO {% user %}"
        )
            .define("privileges", sql(
                privileges.stream()
                    .map(Enum::name)
                    .collect(Collectors.joining(","))
            ))
            .define("object", sql(object))
            .define("user", sql(user))
            .preparedStatement(connection);

        // EXECUTE
        prepared_statement.execute();
    }
}
