package com.smmx.analysis.quetzal.core.http.handlers;

import io.javalin.http.Context;

public interface AuthenticationMethod<T extends ClientProfile> {

    T authenticate(Context ctx);

}
