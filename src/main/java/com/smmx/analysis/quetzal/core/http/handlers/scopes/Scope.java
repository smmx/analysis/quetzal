package com.smmx.analysis.quetzal.core.http.handlers.scopes;

public class Scope {

    private final String name;

    public Scope(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        return this == other;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
