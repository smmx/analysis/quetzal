/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.resources;

import com.smmx.maria.commons.boot.AppConfiguration;
import com.smmx.maria.commons.boot.AppRuntime;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class ResourcesConfiguration {

    // SINGLETON
    private static ResourcesConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static ResourcesConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ResourcesConfiguration();
        }

        return INSTANCE;
    }

    // MEMBER
    private final Path root;

    private ResourcesConfiguration() {
        Map<String, Object> drive_section = dp()
            .get("resources", Collections.emptyMap())
            .asDictionary()
            .notNull()
            .apply(AppConfiguration.getInstance());

        //////////////////////////CONFIG////////////////////////////////////////
        this.root = dp()
            .get("root", "resources")
            .asString()
            .notNull()
            .map(p -> {
                if (p.startsWith("/")) {
                    return FileSystems.getDefault().getPath(p);
                }

                return AppRuntime.getInstance().home().resolve(p);
            })
            .apply(drive_section);
    }

    public Path getRoot() {
        return root;
    }

}
