/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.core.http.errors.responses;

import io.javalin.http.Context;
import io.javalin.http.ExceptionHandler;

/**
 * @author Osvaldo Miguel Colin
 */
public class ErrorResponseExceptionHandler implements ExceptionHandler<ErrorResponseException> {

    @Override
    public void handle(ErrorResponseException error, Context ctx) {
        ctx.status(error.httpStatus());
        ctx.result(error.httpBody());
    }

}
