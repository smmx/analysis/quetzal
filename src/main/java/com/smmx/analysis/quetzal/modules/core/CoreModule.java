/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal.modules.core;

import com.smmx.analysis.quetzal.core.database.configuration.DatabaseConfiguration;
import com.smmx.analysis.quetzal.core.sql.SQLTemplateFile;
import com.smmx.analysis.quetzal.core.sql.SQLTemplateManager;
import com.smmx.maria.commons.boot.AppBootException;
import com.smmx.maria.commons.boot.AppModule;
import com.smmx.maria.commons.database.defaults.DefaultSQLContext;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.sql;

/**
 * @author omiguelc
 */
public class CoreModule extends AppModule {

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(CoreModule.class);

    // MEMBER
    public CoreModule() {
        super("Quetzal Core");
    }

    @Override
    public void configureArgumentParser(ArgumentParser parser) {
        // DO NOTHING
    }

    @Override
    public void configure(Map<String, Object> args) {
        System.setProperty("java.awt.headless", "true");

        // LOG
        LOGGER.info("Using timezone: {}.", System.getProperty("user.timezone"));

        // SET ORACLE PROPERTIES
        System.setProperty("oracle.net.tns_admin", DatabaseConfiguration.getInstance().getTNSAdmin());
        System.setProperty("oracle.jdbc.J2EE13Compliant", "true");

        // LOG
        LOGGER.info("Using tnsadmin: {}.", System.getProperty("oracle.net.tns_admin"));

        // GET RESOURCE PATH
        Path quetzal = null;

        try {
            URL quetzal_url = ClassLoader.getSystemResource("quetzal");

            if (quetzal_url != null) {
                URI quetzal_uri = quetzal_url.toURI();

                if (quetzal_uri.getScheme().equals("jar")) {
                    Map<String, String> env = new HashMap<>();
                    env.put("create", "true");

                    quetzal = FileSystems.newFileSystem(
                        quetzal_uri, env
                    ).getPath("/");
                } else {
                    quetzal = Paths.get(
                        quetzal_uri
                    );
                }
            }
        } catch (URISyntaxException ex) {
            throw new AppBootException("Failed to get resource directory.", ex);
        } catch (IOException ex) {
            throw new AppBootException("Failed to load zip.", ex);
        }

        // SQL DEFINITIONS
        DatabaseConfiguration.getInstance().getSQLDefinitions()
            .forEach((name, value) -> {
                DefaultSQLContext.getInstance().define(name, sql(value));
            });

        // RESOURCE RELATED STUFF
        if (quetzal == null) {
            return;
        }

        // SQL TEMPLATES
        try {
            Files.walk(quetzal)
                .forEach(path -> {
                    // SKIP ROOT
                    if (path.getFileName() == null) {
                        return;
                    }

                    // CHECK FILENAME
                    String filename = path.getFileName().toString();

                    if (!filename.endsWith(".sql")) {
                        return;
                    }

                    LOGGER.info("Loading sql template {}", path.getFileName());

                    // LOAD
                    InputStream is;

                    try {
                        is = Files.newInputStream(path);
                    } catch (IOException ex) {
                        throw new AppBootException("Failed to load sql template.", ex);
                    }

                    SQLTemplateFile sqlt;

                    try {
                        sqlt = new SQLTemplateFile(is);
                    } catch (IOException ex) {
                        throw new AppBootException("Failed to load sql template.", ex);
                    }

                    SQLTemplateManager.getInstance()
                        .registerTemplate(
                            sqlt.getName(),
                            () -> {
                                return sql(sqlt.getSQL())
                                    .registerFields(sqlt.getFields())
                                    .registerParameters(sqlt.getParameters());
                            }
                        );
                });
        } catch (IOException ex) {
            throw new AppBootException("Failed to walk quetzal.", ex);
        }
    }

    @Override
    public void setup() {
        // DO NOTHING
    }

}
