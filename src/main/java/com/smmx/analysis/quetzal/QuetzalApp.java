/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.analysis.quetzal;

import com.smmx.analysis.quetzal.core.executors.EventExecutor;
import com.smmx.analysis.quetzal.core.executors.ScheduledExecutor;
import com.smmx.analysis.quetzal.core.http.Http;
import com.smmx.analysis.quetzal.modules.core.CoreModule;
import com.smmx.maria.commons.boot.ModularApp;

/**
 * @author omiguelc
 */
public class QuetzalApp extends ModularApp {

    public QuetzalApp(String name, String description) {
        super(name, description);

        // ADD MODULES
        this.addModule(new CoreModule());
    }

    @Override
    public void start() {
        Http.getInstance().start();
    }

    @Override
    public void stop() {
        Http.getInstance().stop();
        EventExecutor.getInstance().shutdown();
        ScheduledExecutor.getInstance().shutdown();
    }

}
